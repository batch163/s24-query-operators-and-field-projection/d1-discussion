
// FIND METHOD
	db.collections.find({query}, {field projection})

db.users.find()
	//returns array of documents


/*Query Operators*/

	/*Comparison Operator*/

	// $gt Operator
	db.users.find({age: {$gt: 50}})
		// find documents with age greater than 50

	// $gte Operator
	db.users.find({age: {$gte: 61}})


	// $lt Operator
	db.users.find({age: {$lt: 65}})


	// $lte Operator
	db.users.find({age: {$lte: 65}})


	//$eq
		//Matches values that are equal to a specified value.
		//default operator to query document
	db.users.find({age: {$eq: 65}})

	db.users.find({age: 65})


	// $ne operator
		//Matches all values that are not equal to a specified value.
	db.users.find({age: {$ne: 82}})


	//$in
		//Matches any of the values specified in an array.
	db.users.find(
		{
			lastName: {
				$in: ["Hawking", "Doe"]
			}	
		}
	)

	//find all the documents that contains courses HTML and React
	db.users.find(
		{
			courses: {
				$in: ["HTML", "React"]
			}
		}
	)


	/*Logical Operator*/

	//$and operator
		// Joins query clauses with a logical AND returns all documents that match the conditions of both clauses.
	db.users.find(
		{
			$and: [ 
				{"firstName": "Neil"}, 
				{"age": {$gte: 82}},
				{"active": "status"} 
			]
		}
	)
		//and operator evaluates all fields to be true. if one of the fields is false, it will not return a documents.

	db.users.find(
		{
			"firstName": "Neil",
			"age": {$gte: 82},
			"active": "status"
		}
	)
		//and operator is default behavior of query document because all fields specified in a query must be true


	// $or operator
		//Joins query clauses with a logical OR returns all documents that match the conditions of either clause.

	db.users.find(
		{
			$or: [{"firstName": "Neil"}, {"age": 30}]
		}
	)


	// $regex operator
	db.users.find(
		{
			"firstName": "jane"
		}
	)

	db.users.find(
		{
			"firstName": { 
				$regex: 'jane', 
				$options: 'i' 
			}
		}
	)
	

/* {field projection} parameter 
		- Specifies the fields to return in the documents that match the query filter
*/
	

// Exclusion
	// Specifies the inclusion of a field. Non-zero integers are also treated as true.

	db.users.find(
		{}, 
		{
			"_id": 0,
			"contact": 0,
			"department": 0,
			"courses": 0,
			"status": 0,
			"age": 0,
			"active": 0,
			"isAdmin": 0
		}
	)


// Inclusion
	// Specifies the inclusion of a field. Non-zero integers are also treated as true.
	db.users.find(
		{}, 
		{
			"firstName": 1,
			"lastName": 1,
			"_id": 0
		}
	)


	db.users.find(
		{
			"age": {$gte: 50}
		},
		{
			"firstName": 1,
			"_id": 0
		}
	)


	//look for documents under hr dept and return their first and lastname plus phone number only
	db.users.find(
		{"department": "HR"},
		{"firstName": 1, "lastName": 1, "contact.phone": 1, "_id": 0}
	)